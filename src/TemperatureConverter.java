
/**
 * @Class - CO-CP Alpha 1
 * @Student - Don Moore
 * @Assignment - Temperature Converter app
 * @Task - User is asked to enter a temperature and its temperature scale in Celsius, Kelvin, or Fahrenheit.
 *       - The program will convert the entered temperature into the other two remaining temperature scales.
 *       - The program will print the original temperature and the converted temperatures results on separate lines.
 */
import java.util.Scanner;

public class TemperatureConverter {

    // Method for printing output of calculations
    public static void printAnswer(double temperature, String scaleEntered, double firstConvertedTemperature, String firstConvertedScale, double secondConvertedTemperature, String secondConvertedScale) {
        System.out.println("\nThe input temperature is " + temperature + " degrees " + scaleEntered + ".");
        System.out.println("The temperature in " + firstConvertedScale + " is " + firstConvertedTemperature + " degrees.");
        System.out.println("The temperature in " + secondConvertedScale + " is " + secondConvertedTemperature + " degrees.");
    }

    public static void main(String[] args) {

        Scanner console = new Scanner(System.in);

        // Ask user to enter temperature
        System.out.print("Enter temperature in degrees: ");
        double temperature = console.nextDouble();

        // Ask user to enter temp scale
        System.out.println("\nEnter the number that matches the temperature scale the temperature is measured in.\n1: Celsius\n2: Fahrenheit\n3: Kelvin");
        int scale = console.nextInt();

        // Convert temp to other temp scales
        if (scale == 1) {
            double celsiusToFahrenheit = Math.round((((temperature * 9/5) + 32) * 100.0)/100.0);
            double celsiusToKelvin = Math.round(((temperature + 273.15) * 100.0)/100.0);

            // Use Print answer method to print answer
            printAnswer(temperature, "Celsius", celsiusToFahrenheit, "Fahrenheit", celsiusToKelvin, "Kelvin");

        } else if (scale == 2) {
            double fahrenheitToCelsius = Math.round((((temperature - 32) * 5/9) * 100.0)/100.0);
            double fahrenheitToKelvin = Math.round((((temperature - 32) * 5/9 + 273.15) * 100.0)/100.0);

            // Use Print answer method to print answer
            printAnswer(temperature, "Fahrenheit", fahrenheitToCelsius, "Celsius", fahrenheitToKelvin, "Kelvin");

        } else if (scale == 3) {
            double kelvinToCelsius = Math.round(((temperature - 273.15) * 100.0)/100.0);
            double kelvinToFahrenheit = Math.round((((temperature - 273.15) * 5/9 + 32) * 100.0)/100.0);

            // Use Print answer method to print answer
            printAnswer(temperature, "Kelvin", kelvinToCelsius, "Celsius", kelvinToFahrenheit, "Fahrenheit");
        }
    } // end main()
} // end class
